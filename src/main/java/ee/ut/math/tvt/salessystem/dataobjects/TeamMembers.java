package ee.ut.math.tvt.salessystem.dataobjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class TeamMembers {
    Properties properties;
    String leaderName;
    String teamName;
    String leaderEmail;
    String teamMembers;
    String teamImage;

    /**
     * Fetch Team members info from properties file
     */
    public TeamMembers() throws IOException {

        properties = new Properties();
        String result = "";
        String propFileName = "application.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Setting all the variables needed to get team information.
        this.teamImage = properties.get("team.imageURI").toString();
        this.leaderName = properties.get("team.leader").toString();
        this.teamName = properties.get("team.name").toString();
        this.leaderEmail = properties.get("team.email").toString();
        this.teamMembers = properties.get("team.members").toString();


    }

    public String getTeamImage() {
        return teamImage;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getLeaderEmail() {
        return leaderEmail;
    }

    public String getTeamMembers() {
        return teamMembers;
    }
}
