package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Purchase> purchaseHistory;
    //private final TeamMembers teamMembers;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.purchaseHistory = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItemByID(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItemByName(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public void savePurchase(List<SoldItem> soldItems) {
        // Make a history item list of all the sold items
        List<HistoryItem> purchaseContents = new ArrayList<>();
        for (SoldItem soldItem: soldItems) {
            purchaseContents.add(new HistoryItem(soldItem));
        }
        // Make the history item list into a purchase and save it
        purchaseHistory.add(new Purchase(purchaseContents));
    }


    @Override
    public List<Purchase> findPurchaseHistory() {
        return purchaseHistory;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem item) {
        boolean existed = false;
        for (StockItem mutable : stockItemList) {
            if (mutable.getId() == item.getId()) {
                mutable.setQuantity(item.getQuantity());
                existed = true;
            }
        }
        if (!existed) {
            stockItemList.add(item);
        }
    }

    @Override
    public void addExistingStockItem(StockItem item) {
        for (StockItem stockItem : stockItemList){
            if (stockItem.getId().equals(item.getId())){
                stockItem.setPrice(item.getPrice());
                stockItem.setQuantity(item.getQuantity());
                break;
            }
        }
    }

    @Override
    public List<Purchase> getBetweenDates(LocalDate startDate, LocalDate endDate) {
        List<Purchase> result = new ArrayList<>();
        for (Purchase purchase: purchaseHistory) {
            LocalDate purchaseLocalDate = purchase.getLocalDate();
            if (!(purchaseLocalDate.isBefore(startDate)) && !purchaseLocalDate.isAfter(endDate)) {
                result.add(purchase);
            }
        }
        return result;
    }

    @Override
    public List<Purchase> getLast10() {
        if (purchaseHistory.size() == 0) {
            return new ArrayList<>();
        }
        List<Purchase> result = new ArrayList<>();

        for (int i = purchaseHistory.size() - 1; i > purchaseHistory.size() - 11 && i >= 0 ; i--) {
            result.add(purchaseHistory.get(i));
        }
        return result;
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void update() {

    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }
}
