package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.List;

public class WarehouseStorage {
    private final SalesSystemDAO dao;
    private List<StockItem> items;

    public WarehouseStorage(SalesSystemDAO dao) {
        this.dao = dao;
        this.items = dao.findStockItems();
    }

    public void addItem(StockItem item){
        boolean add = true;
        dao.beginTransaction();
        items = dao.findStockItems();
        for (StockItem stockItem : items){
            if (stockItem.getId().equals(item.getId())){
                item.setQuantity(stockItem.getQuantity() + item.getQuantity());
                dao.addExistingStockItem(item);
                dao.commitTransaction();
                add = false;
                break;
            }
        }
        if (add){
            dao.saveStockItem(item);
            dao.commitTransaction();
        }
    }

    public boolean contains(StockItem item){
        for (StockItem stockItem : items){
            if (stockItem.getId().equals(item.getId())){
                return true;
            }
        }
        return false;
    }
}
