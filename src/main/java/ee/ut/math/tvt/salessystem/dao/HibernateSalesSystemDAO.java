package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;
    private List<StockItem> stockItemList;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Cabbage", "Green ball", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        beginTransaction();
        for (StockItem item : items) {
            saveStockItem(item);
        }
        commitTransaction();
        update();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public void update() {
        stockItemList = em.createQuery("From StockItem", StockItem.class).getResultList();
    }

    @Override
    public List<StockItem> findStockItems() {
        Query query = em.createQuery("Select si FROM StockItem si");
        return query.getResultList();
    }

    @Override
    public List<Purchase> findPurchaseHistory() {
        Query query = em.createQuery("SELECT si From Purchase si");
        return query.getResultList();
    }

    @Override
    public StockItem findStockItemByID(long id) {
        //We use stockItemList to have faster querys to data. We update the list from
        //time to time in order to have all the fresh data.
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItemByName(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public void savePurchase(List<SoldItem> soldItems) {
        List<HistoryItem> purchaseContents = new ArrayList<>();
        for (SoldItem soldItem : soldItems) {
            purchaseContents.add(new HistoryItem(soldItem));
        }
        Purchase purchase = new Purchase(purchaseContents);
        em.merge(purchase);

    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.merge(item);
    }

    @Override
    public void saveStockItem(StockItem item) {
        em.merge(item);
        update();

    }

    @Override
    public void addExistingStockItem(StockItem item) {
        StockItem dataItem = em.find(StockItem.class, item.getId());
        dataItem.setPrice(item.getPrice());
        dataItem.setQuantity(item.getQuantity());
        update();

    }

    @Override
    public List<Purchase> getBetweenDates(LocalDate startDate, LocalDate endDate) {
        List<Purchase> resultList = em
                .createQuery("select p from Purchase p where p.timestamp BETWEEN :startDate  AND :endDate")
                .setParameter("startDate",java.sql.Date.valueOf(startDate))
                .setParameter("endDate", java.sql.Date.valueOf(endDate))
                .getResultList();
        return resultList;
    }

    @Override
    public List<Purchase> getLast10() {
        List<Purchase> resultList =  em.createQuery("select p from Purchase p order by p.timestamp desc").setMaxResults(10).getResultList();
        return resultList;
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }
}
