package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "HISTORY_ITEM")
public class HistoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "history_item_id", referencedColumnName = "id")
    private SoldItem soldItem;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price")
    private double price;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Purchase> purchases;

    public HistoryItem() {

    }

    public HistoryItem(SoldItem soldItem) {
        this.id = soldItem.getId();
        this.soldItem = soldItem;
        this.name = soldItem.getName();
        this.quantity = soldItem.getQuantity();
        this.price = soldItem.getPrice();
    }

    public Long getId() {
        return id;
    }

    public double getSum() {
        return this.price * this.quantity;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SoldItem getSoldItem() {
        return soldItem;
    }

    public void setSoldItem(SoldItem soldItem) {
        this.soldItem = soldItem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "HistoryItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
