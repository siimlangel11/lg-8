package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PURCHASE")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<HistoryItem> contents;

    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "date")
    private String date;

    @Column(name = "time")
    private String time;

    @Column(name = "total")
    private double total = 0;

    @Transient
    private SimpleDateFormat sdfDate;

    @Transient
    private SimpleDateFormat sdfTime;


    public Purchase() {

    }

    public Purchase(List<HistoryItem> contents) {
        this.contents = contents;
        sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        sdfTime = new SimpleDateFormat("HH:mm:ss.SSS");
        this.timestamp = new Date();
        this.date = sdfDate.format(timestamp);
        this.time = sdfTime.format(timestamp);


        for (HistoryItem item : contents) {
            this.total += item.getSum();
        }
    }

    public Long getId() {
        return this.id;
    }

    public List<HistoryItem> getContents() {
        return contents;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getDate() {
        return date;
    }

    public LocalDate getLocalDate() {
        return timestamp.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public String getTime() {
        return time;
    }

    public double getTotal() {
        return total;
    }
}
