package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    private static final Logger log = LogManager.getLogger(ShoppingCart.class);


    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public boolean addItem(SoldItem item) {
        if (item.getQuantity() < 1) {
            return false;
        }
        boolean canAddExisting = false;
        boolean cantAdd = false;
        for (SoldItem currentItem : items) {
            if (currentItem.getId().equals(item.getId())) {
                if (item.getStockItem().getQuantity() >= currentItem.getQuantity() + item.getQuantity()) {
                    canAddExisting = true;
                    item.setId(item.getStockItem().getId());
                    currentItem.setQuantity(item.getQuantity() + currentItem.getQuantity());
                    return true;
                }
                cantAdd = true;
            }



        }
        if (!canAddExisting && !cantAdd) {
            if (item.getStockItem().getQuantity() >= item.getQuantity()) {
                item.setId(item.getStockItem().getId());
                log.debug("Added SoldItem: " + item.getName());
                items.add(item);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public List<SoldItem> getAll() {
        return this.items;
    }

    public void cancelCurrentPurchase() {
        this.items.clear();
    }

    public void submitCurrentPurchase() {
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            for (SoldItem item : this.items) {
                dao.saveSoldItem(item);
                StockItem stockItem = item.getStockItem();
                stockItem.setQuantity(stockItem.getQuantity() - item.getQuantity());
                dao.saveStockItem(stockItem);
                log.debug("Dao saved stockItem: " + stockItem.getName());
            }
            // Save purchase
            dao.update();
            dao.savePurchase(this.items);
            dao.commitTransaction();
            this.items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public boolean removeItem(StockItem stockItem, int quantity) {
        for (SoldItem item : items) {
            if (item.getStockItem().getId().equals(stockItem.getId())) {
                int newQuantity = item.getQuantity() - quantity;
                if (newQuantity < 0){
                    return false;
                }
                if (newQuantity == 0) {
                    items.remove(item);
                }
                item.setQuantity(newQuantity);
                return true;
            }
        }
        return false;
    }
}
