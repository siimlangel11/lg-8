package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.TeamMembers;

public class TeamInfo {
    private final SalesSystemDAO dao;
    private final TeamMembers teamMembers;

    public TeamInfo(SalesSystemDAO dao) {
        this.dao = dao;
        //TODO: Fetch members from database.
        this.teamMembers = null;
    }
}
