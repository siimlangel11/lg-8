package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HistoryStorage {

    private final SalesSystemDAO dao;
    private final List<HistoryItem> items = new ArrayList<>();
    private List<Purchase> purchases;

    public HistoryStorage(SalesSystemDAO dao) {
        this.dao = dao;
        this.purchases = dao.findPurchaseHistory();
    }

    public List<Purchase> getAllPurchases() {
        purchases = dao.findPurchaseHistory();
        return purchases;
    }

    public List<Purchase> getBetweenDates(LocalDate startDate, LocalDate endDate) {
        if (startDate == null || endDate == null) return new ArrayList<>();
        return dao.getBetweenDates(startDate, endDate);
    }

    public List<Purchase> getLast10() {
        return dao.getLast10();
    }

}
