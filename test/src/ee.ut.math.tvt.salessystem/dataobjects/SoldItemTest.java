package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import org.junit.Test;

import static org.junit.Assert.*;

public class SoldItemTest {
    @Test(expected = SalesSystemException.class)
    public void creatingItemWithNegativeQuantity() {
        StockItem item = new StockItem((long) 5, "Something","Else", 10, 10);
        SoldItem soldItem = new SoldItem(item, -10);
    }

}