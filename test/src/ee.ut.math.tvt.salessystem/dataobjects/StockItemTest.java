package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import org.junit.Test;

import static org.junit.Assert.*;

public class StockItemTest {

    @Test
    public void getDescriptionTest() {
        StockItem item = new StockItem((long) 12, "Lettuce", "Green", 12,17);
        assertEquals("Green", item.getDescription());
    }

    @Test
    public void setDescriptionTest() {
        StockItem item = new StockItem();
        item.setDescription("Yellow");
        assertEquals("Yellow", item.getDescription());
    }

    @Test
    public void getNameTest() {
        StockItem item = new StockItem((long) 12, "Lettuce", "Green", 12,17);
        assertEquals("Lettuce", item.getName());
    }
     @Test
    public void setNameTest() {
        StockItem item = new StockItem();
        item.setName("Garbage");
        assertEquals("Garbage", item.getName());
     }
     @Test(expected = SalesSystemException.class)
    public void creatingItemWithNegativePrice() {
        StockItem item = new StockItem((long) 5, "Something","Else", -5, 10);
     }

    @Test(expected = SalesSystemException.class)
    public void creatingItemWithNegativeQuantity() {
        StockItem item = new StockItem((long) 5, "Something","Else", 5, -10);
    }


}