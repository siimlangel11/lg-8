package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import static org.junit.Assert.*;

public class WarehouseStorageTest {
    private SalesSystemDAO dao;
    private WarehouseStorage warehouseStorage;


    @Before
    public void setUp() throws Exception {
        this.dao = new InMemorySalesSystemDAO();
        this.warehouseStorage = new WarehouseStorage(dao);
    }

    @Test
    public void addExistingItemTest() {
        List<StockItem> allItems = dao.findStockItems();
        StockItem chosen = allItems.get(0);
        int currentQuantity = chosen.getQuantity();
        //item has the quantity 2.
        //If adding is successful, the allItems.get(0) will have an incremented quantity.
        StockItem item = new StockItem(chosen.getId(), chosen.getName(), chosen.getDescription(), chosen.getPrice(), 2);
        warehouseStorage.addItem(item);
        assertEquals(currentQuantity + 2, allItems.get(0).getQuantity());
    }

    @Test
    public void addingNewItemTest() {
        StockItem item = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        List<StockItem> items = dao.findStockItems();
        //Checking that dao is connected to adding the item. If not, dao cant contain the stockItem.
        warehouseStorage.addItem(item);
        assertTrue(items.contains(item));
    }

    @Test //Checking if the .saveStockItem() methog is invoked
    public void addingNewItemTestMockito() {
        SalesSystemDAO mockDao = spy(InMemorySalesSystemDAO.class);
        WarehouseStorage mockHouse = new WarehouseStorage(mockDao);
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        //Checking that dao is connected to adding the item. If not, dao cant contain the stockItem.
        mockHouse.addItem(stockItem);
        verify(mockDao, times(1)).saveStockItem(stockItem);
    }

    @Test //Checking if the .saveStockItem() method is not invoked.
    public void addingExistingItemTestMockito() {
        SalesSystemDAO mockDao = spy(InMemorySalesSystemDAO.class);
        WarehouseStorage mockHouse = new WarehouseStorage(mockDao);
        List<StockItem> items = mockDao.findStockItems();
        StockItem chosen = items.get(0); //Taking an element that exists for sure.
        mockHouse.addItem(chosen);
        verify(mockDao, times(0)).saveStockItem(chosen);
    }

    @Test
    public void invocationTestInMemory() {
        InMemorySalesSystemDAO mockDao = mock(InMemorySalesSystemDAO.class);
        WarehouseStorage cart = new WarehouseStorage(mockDao);
        //Initiating the method in which the two methods reside.
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        cart.addItem(stockItem);
        //Checking if both of the methods are only called once.
        verify(mockDao, times(1)).beginTransaction();
        verify(mockDao, times(1)).commitTransaction();
    }



    @Test
    public void invocationOrderTestInMemory() {
        InMemorySalesSystemDAO mockDao = mock(InMemorySalesSystemDAO.class);
        WarehouseStorage cart = new WarehouseStorage(mockDao);
        //Initiating the method in which the two methods reside.
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        cart.addItem(stockItem);
        //Checking that the order of method calls is begin first and commit second.
        InOrder inOrder = inOrder(mockDao);
        inOrder.verify(mockDao).beginTransaction();
        inOrder.verify(mockDao).commitTransaction();
    }


}