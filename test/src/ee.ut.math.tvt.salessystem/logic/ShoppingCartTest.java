package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

public class ShoppingCartTest {
    private SalesSystemDAO dao;
    private ShoppingCart cart;

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
        this.cart = new ShoppingCart(dao);
    }

    @Test
    public void testAddingNewItem() {
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        SoldItem soldItem = new SoldItem(stockItem, 10);
        //Adding item
        cart.addItem(soldItem);
        //Will return true if cart contains the added item.
        assertTrue(cart.getAll().contains(soldItem));
    }

    @Test
    public void testAddingExistingItem() {
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        SoldItem soldItem = new SoldItem(stockItem, 10);
        //Adding item
        cart.addItem(soldItem);
        SoldItem existing = new SoldItem(stockItem, 3);
        cart.addItem(existing);
        SoldItem cartContent = cart.getAll().get(0);
        //Will return true if cart contains the added item with the right amount.
        assertEquals(13, (int) cartContent.getQuantity());
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity() {
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        SoldItem soldItem = new SoldItem(stockItem, -1);
        //Adding item
        cart.addItem(soldItem);
        //Will return true if cart contains the added item.
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        SoldItem soldItem = new SoldItem(stockItem, 19);
        //Adding item
        //Will return true if cart contains the added item.
        assertFalse(cart.addItem(soldItem));
    }
    @Test
    public void testAddingItemWithSumTooLarge() {
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        SoldItem soldItem = new SoldItem(stockItem, 12);
        //Adding item
        //Will return true if cart contains the added item.
        cart.addItem(soldItem); //Adding to the cart a quantity of 12.

        assertFalse(cart.addItem(soldItem)); //Adding again 12 which exceeds the 17.
    }

    @Test
    public void removeItemTest() {
        StockItem stockItem = new StockItem((long) 12, "Lettuce", "Green", 12, 17);
        SoldItem soldItem = new SoldItem(stockItem, 10);
        //Adding item
        cart.addItem(soldItem);
        cart.removeItem(soldItem);
        //Asserting that the soldItem does not exist in the cart any more.
        assertFalse(cart.getAll().contains(soldItem));
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        int itemId = 12;
        StockItem stockItem = new StockItem((long) itemId, "Lettuce", "Green", 12, 17);
        //Saving a new item, so I would not have to rely on any items added in the past.
        dao.saveStockItem(stockItem); //Using the predefined variables of the class.
        //currentStock will later be compared to the stockitem quantity in dao.
        int currentStock = stockItem.getQuantity();

        SoldItem sold = new SoldItem(stockItem, 3);
        cart.addItem(sold);
        cart.submitCurrentPurchase();
        //Just checking whether the stock that we saved before is bigger that the current one.
        assertTrue(currentStock > dao.findStockItemByID(itemId).getQuantity());
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        InMemorySalesSystemDAO mockDao = mock(InMemorySalesSystemDAO.class);
        ShoppingCart cart = new ShoppingCart(mockDao);
        //Initiating the method in which the two methods reside.
        cart.submitCurrentPurchase();
        //Checking if both of the methods are only called once.
        verify(mockDao, times(1)).beginTransaction();
        verify(mockDao, times(1)).commitTransaction();

        //Order check. First comes begin, then commit.
        InOrder inOrder = inOrder(mockDao);
        inOrder.verify(mockDao).beginTransaction();
        inOrder.verify(mockDao).commitTransaction();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        //Initiating variables
        int itemId = 12;
        StockItem stockItem = new StockItem((long) itemId, "Lettuce", "Green", 12, 17);
        //Saving a new item, so I would not have to rely on any items added in the past.
        dao.saveStockItem(stockItem);
        //currentStock will later be compared to the stockitem quantity in dao.

        SoldItem sold = new SoldItem(stockItem, 3);
        cart.addItem(sold);
        cart.submitCurrentPurchase();
        List<Purchase> history = dao.findPurchaseHistory();
        assertFalse(history.isEmpty());
        assertSame(history.get(0).getContents().get(0).getId(), sold.getId());

    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        int itemId = 12;
        StockItem stockItem = new StockItem((long) itemId, "Lettuce", "Green", 12, 17);
        //Saving a new item, so I would not have to rely on any items added in the past.
        dao.saveStockItem(stockItem);
        //currentStock will later be compared to the stockitem quantity in dao.

        SoldItem sold = new SoldItem(stockItem, 3);
        //Taking the time before the submit.
        Date before = new Date();
        cart.addItem(sold);
        cart.submitCurrentPurchase();
        List<Purchase> history = dao.findPurchaseHistory();
        //Taking the time after the submit.
        Date after = new Date();
        //Taking the time of the submit.
        Date timestamp = history.get(0).getTimestamp();
        //Comparing that before < timestamp < after.
        assertTrue(before.compareTo(timestamp) <= 0 && timestamp.compareTo(after) <= 0);
    }

    @Test
    public void testCancellingOrder() {
        dao.saveStockItem(new StockItem((long) 15, "Potatoes", "Yellow", 12, 10));
        dao.saveStockItem(new StockItem((long) 16, "Rice", "White", 12, 20));

        StockItem beforeCancel = dao.findStockItemByID(15);
        cart.addItem(new SoldItem(beforeCancel, 4));
        cart.cancelCurrentPurchase();
        assertTrue(cart.getAll().isEmpty()); //Checking that the cart is empty

        StockItem afterCancel = dao.findStockItemByID(16);
        cart.addItem(new SoldItem(afterCancel, 10));
        cart.submitCurrentPurchase();
        //Taking the single added item and saving it to Purchase.
        Purchase submittedItem = dao.findPurchaseHistory().get(0);

        assertEquals(1, submittedItem.getContents().size()); //Checking that the purchase only has 1 item.
        //Checking that the beforeItem ID is not the same that the single item in the purchase has.
        assertNotEquals(submittedItem.getContents().get(0).getId(), beforeCancel.getId());

    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        int itemId = 12;
        StockItem stockItem = new StockItem((long) itemId, "Lettuce", "Green", 12, 17);
        //Saving a new item, so I would not have to rely on any items added in the past.
        dao.saveStockItem(stockItem); //Using the predefined variables of the class.
        //currentStock will later be compared to the stockitem quantity in dao.
        int currentStock = stockItem.getQuantity();

        SoldItem sold = new SoldItem(stockItem, 3);
        cart.addItem(sold);
        cart.cancelCurrentPurchase();
        assertEquals(currentStock, dao.findStockItemByID(itemId).getQuantity());
    }

}