# Team LG-8:

1. <Marten Türk>
2. <Siim Langel>
3. <Henrik Tamm>

## Homework 1:

Link to wiki: https://bitbucket.org/siimlangel11/lg-8/wiki/Homework%201

Link to JIRA: https://lg-8.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=UFN&selectedIssue=UFN-37

## Homework 2:

Link to wiki: https://bitbucket.org/siimlangel11/lg-8/wiki/Homework%202

Link to JIRA: https://lg-8.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=UFN&selectedIssue=UFN-112

## Homework 3:

<Links to the solution>

## Homework 4:

https://bitbucket.org/siimlangel11/lg-8/wiki/Homework%204

## Homework 5:

https://bitbucket.org/siimlangel11/lg-8/wiki/Homework%205

Git tag link: https://bitbucket.org/siimlangel11/lg-8/commits/tag/homework-5

## Homework 6:

https://bitbucket.org/siimlangel11/lg-8/wiki/Homework%206

Link to tag: https://bitbucket.org/siimlangel11/lg-8/commits/tag/homework-6

## Homework 7:

Link to wiki : https://bitbucket.org/siimlangel11/lg-8/wiki/Homework%207

## Functional testing plan

<https://docs.google.com/spreadsheets/d/1v3LkC7heHlKjo2khzoPBCBOCKi9XnGGhoH0wIH9Jh0k/edit?usp=sharing>



We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
