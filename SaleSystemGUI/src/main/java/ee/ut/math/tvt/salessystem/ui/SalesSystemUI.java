package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.HistoryStorage;
import ee.ut.math.tvt.salessystem.logic.WarehouseStorage;
import ee.ut.math.tvt.salessystem.ui.controllers.*;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.application.Application;

import javafx.scene.Group;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;


/**
 * Graphical user interface of the sales system.
 */
public class SalesSystemUI extends Application {

    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);
    private static final ControllerUtility utils = new ControllerUtility();

    private final HistoryStorage history;
    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;
    private final WarehouseStorage warehouse;

    public SalesSystemUI() {
        dao = new InMemorySalesSystemDAO();
        shoppingCart = new ShoppingCart(dao);
        history = new HistoryStorage(dao);
        warehouse = new WarehouseStorage(dao);
    }
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, 600, 500, Color.WHITE);

        // Only show login on application start
        BorderPane borderPane = new BorderPane();
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        try {
            borderPane.setCenter(utils.loadControls("login.fxml", new LoginController(dao, shoppingCart, history, warehouse)));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        root.getChildren().add(borderPane);

        primaryStage.setTitle("Sales system");
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(getClass().getResource("ApplicationTheme.css").toExternalForm());

        log.info("Salesystem GUI started");
    }
}


