package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.WarehouseStorage;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;
    private final WarehouseStorage warehouse;
    private static final Logger log = LogManager.getLogger(StockController.class);


    @FXML
    private Button addItemButton;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private Button refreshButton;
    @FXML
    private javafx.scene.control.TextField barCodeField;
    @FXML
    private javafx.scene.control.TextField quantityField;
    @FXML
    private javafx.scene.control.TextField nameField;
    @FXML
    private javafx.scene.control.TextField priceField;
    @FXML
    private Button confirmButton;
    @FXML
    private Button fillInputsButton;

    public StockController(SalesSystemDAO dao, WarehouseStorage warehouse) {
        this.dao = dao;
        this.warehouse = warehouse;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        disableInputs(false);
        refreshStockItems();
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    @FXML
    public void addItemsButtonClicked(){ disableInputs(true);}

    @FXML
    private void fillInputsButtonPressed(){
        fillInputs(getStockItemByBarcode());
    }

    @FXML
    public void textFieldListener() {
        //TODO: everything except the name can be edited.
    }

    @FXML
    public void addItemEventHandler() {
        boolean add = true;
        StockItem item = new StockItem();
        ArrayList<String> wrong = new ArrayList<>();
        try{
            if (Long.parseLong(barCodeField.getText()) >= 0L){
                item.setId(Long.parseLong(barCodeField.getText()));
            } else {
                add = false;
            }
        } catch (NumberFormatException e){
            wrong.add("ID or the product");
            add = false;
        }

        try {
            if (Integer.parseInt(quantityField.getText()) > 0){
                item.setQuantity(Integer.parseInt(quantityField.getText()));
            } else {
                add = false;
            }
        } catch (NumberFormatException e){
            wrong.add("Quantity of the product.");
            add = false;
        }

        try {
            if (Double.parseDouble(priceField.getText()) >= 0.0){
                item.setPrice(Double.parseDouble(priceField.getText()));
            } else {
                add = false;
            }
        } catch (NumberFormatException e){
            wrong.add("Price of the product.");
            add = false;
        }

        item.setName(nameField.getText());
        if (nameField.getText().equals("")) {
            wrong.add("Name field");
        }
        if (add){
            log.info("Added item to warehouse: " + item.getName());
            warehouse.addItem(item);
            disableInputs(false);
            resetProductField();
            refreshStockItems();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!");
            alert.setHeaderText("Some of the fields you've entered are incorrect.");
            String message = "";
            for (String error : wrong) {
                message += error + "\n";
            }
            alert.setContentText(message);
            alert.showAndWait();
        }
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
    }

    private void disableInputs(boolean disable) {
        refreshButton.setDisable(disable);
        addItemButton.setDisable(disable);
        confirmButton.setDisable(!disable);
        fillInputsButton.setDisable(!disable);
        barCodeField.setDisable(!disable);
        nameField.setDisable(!disable);
        quantityField.setDisable(!disable);
        priceField.setDisable(!disable);
    }

    private void resetProductField(){
        barCodeField.setText("");
        nameField.setText("");
        quantityField.setText("1");
        priceField.setText("");
    }

    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItemByID(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private void fillInputs(StockItem item){
        if (item != null){
            priceField.setText(String.valueOf(item.getPrice()));
            nameField.setText(item.getName());
            nameField.setDisable(true);
        }
    }
}
