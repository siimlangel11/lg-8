package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.*;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private ComboBox nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private Button fillInputsButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private Button removeItemButton;
    @FXML
    private javafx.scene.control.ComboBox itemComboBox;
    @FXML
    private Button editItemButton;

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        disableInputs(true);
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        this.nameField.getEditor().textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.length() > 0){
                    changeNameFieldOptions(newValue);
                } else {
                    nameField.hide();
                }
            }
        });
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            disableInputs(false);
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs(true);
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    @FXML
    private void fillInputsButtonPressed() {
        if (barCodeField.getText().length() > 0){
            nameField.setEditable(false);
            fillInputs(getStockItemByBarcode());
        } else {
            barCodeField.setEditable(false);
            fillInputs(getStockItemByName());
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs(false);
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            int quantity;
            try {
                quantity = Integer.parseInt(quantityField.getText());
            } catch (NumberFormatException e) {
                quantity = 1;
            }

            if (stockItem.getQuantity() >= quantity) {
                try {
                    SoldItem toBeAdded = new SoldItem(stockItem, quantity);
                    boolean wasAdded = shoppingCart.addItem(toBeAdded);
                    purchaseTableView.refresh();
                    resetProductField();
                    if (!wasAdded) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Error");
                        alert.setHeaderText("Out of stock!");
                        alert.setContentText("We don't have as much of the product you need in the warehouse.");
                        quantityField.setText("1");
                        alert.showAndWait();
                    }
                } catch (SalesSystemException e) {
                    throwQuantityError();
                }


            } else {
                throwQuantityError();
            }
        }
    }

    @FXML
    public void removeItemEventHandler() {
        StockItem stockItem = getStockItemByBarcode();

        if (stockItem != null) {
            int quantity;
            try {
                quantity = Integer.parseInt(quantityField.getText());
            } catch (NumberFormatException e) {
                quantity = 1;
            }
            boolean canRemove = shoppingCart.removeItem(stockItem, quantity);
            if (canRemove){
                purchaseTableView.refresh();
                resetProductField();
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Cannot remove this quantity");
                alert.setContentText("Cart does not contain enough items to remove");
                resetProductField();
                alert.showAndWait();
            }
        }
    }

    private void throwQuantityError() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error");
        alert.setHeaderText("Some of the fields you've entered are incorrect.");
        alert.setContentText("Quantity must be bigger than 0 or we don't have enough of the product you" +
                " are looking for.");
        quantityField.setText("1");
        alert.showAndWait();
    }

    // switch UI to the state that allows to initiate new purchase or to proceed with the purchase
    private void disableInputs(boolean disable) {
        resetProductField();
        cancelPurchase.setDisable(disable);
        submitPurchase.setDisable(disable);
        newPurchase.setDisable(!disable);
        disableProductInputField(disable);
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductInputField(boolean disable) {
        addItemButton.setDisable(disable);
        removeItemButton.setDisable(disable);
        fillInputsButton.setDisable(disable);
        barCodeField.setDisable(disable);
        quantityField.setDisable(disable);
        nameField.setDisable(disable);
        nameField.setEditable(!disable);
        priceField.setEditable(disable);
        priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setEditable(true);
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setEditable(true);
        nameField.getItems().removeAll(nameField.getItems());
        nameField.setValue(null);
        priceField.setText("");
    }

    private void fillInputs(StockItem item) {
        if (item != null) {
            barCodeField.setText(String.valueOf(item.getId()));
            nameField.setValue(item.getName());
            priceField.setText(String.valueOf(item.getPrice()));
            priceField.setEditable(false);
        } else {
            resetProductField();
        }
    }

    private void changeNameFieldOptions(String value){
        List<String> options = new ArrayList<>();
        List<StockItem> items = dao.findStockItems();
        for (StockItem item : items){
            StockItem comparableStockItem = item;
            if (comparableStockItem.getName().toLowerCase().contains(value.toLowerCase())){
                options.add(comparableStockItem.getName());
            }
        }
        if (options.size() != 0){
            nameField.setItems(FXCollections.observableList(options));
            nameField.show();
        } else {
            nameField.hide();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItemByID(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private StockItem getStockItemByName() {
        try {
            String name = String.valueOf(nameField.getValue());
            return dao.findStockItemByName(name);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public void textfieldListener() {
        //TODO: Only the barcode or name can be entered at a time. If one is entered and the other is not empty, the filled one will be emptied.
    }

    @FXML
    public void editItemEventHandler(SoldItem item) {
        //TODO: After pressing the edit button on an item in the cart, the cashier can change the amount of item.
    }
}
