package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.HistoryStorage;
import ee.ut.math.tvt.salessystem.ui.SalesSystemUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {
    private final SalesSystemDAO dao;
    private final HistoryStorage history;
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    @FXML
    private Button showBetweenDatesBtn;

    @FXML
    private Button showLast10Btn;

    @FXML
    private Button showAllBtn;

    @FXML
    private DatePicker startDate;

    @FXML
    private DatePicker endDate;

    @FXML
    private TableView<Purchase> historyTableView;

    @FXML
    private TableView<HistoryItem> purchaseTableView;


    public HistoryController(SalesSystemDAO dao, HistoryStorage history) {
        this.dao = dao;
        this.history = history;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.debug("HistoryController initialized");
    }

    @FXML
    void showAllBtnEventHandler(ActionEvent event) {
        List<Purchase> allPurchases = history.getAllPurchases();
        historyTableView.setItems(FXCollections.observableList(allPurchases));
      }

    @FXML
    void showBetweenDatesBtnEventHandler(ActionEvent event) {
        LocalDate start = startDate.getValue();
        LocalDate end = endDate.getValue();
        List<Purchase> purchasesBetweenDates = history.getBetweenDates(start, end);
        historyTableView.setItems(FXCollections.observableList(purchasesBetweenDates));
    }

    @FXML
    void showLast10BtnEventHandler(ActionEvent event) {
        List <Purchase> purchasesLast10 = history.getLast10();
        historyTableView.setItems(FXCollections.observableList(purchasesLast10));
    }

    @FXML
    void historyTableCellClicked(MouseEvent event) {
        Purchase selectedPurchase = historyTableView.getSelectionModel().getSelectedItem();
        if (selectedPurchase != null) {
            purchaseTableView.setItems(FXCollections.observableList(selectedPurchase.getContents()));
        }
    }
}
