package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dataobjects.TeamMembers;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {
    Properties properties;
    private static final Logger log = LogManager.getLogger(TeamController.class);


    @FXML
    ImageView imageView;
    @FXML
    AnchorPane imagePane;
    @FXML
    Text teamNameText;
    @FXML
    Text teamLeaderText;
    @FXML
    Text teamLeaderEmailText;
    @FXML
    Text teamMembersText;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TeamMembers teamInfo = null;
        try {
            teamInfo = new TeamMembers();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Set the team image
            Image img = new Image(new FileInputStream(teamInfo.getTeamImage()));
            imageView.setImage(img);
            imageView.setFitHeight(imagePane.getPrefHeight());
            imageView.setFitWidth(imagePane.getPrefWidth());

            // Set team info Text fields' values
            teamNameText.setText(teamInfo.getTeamName());
            teamLeaderText.setText(teamInfo.getLeaderName());
            teamLeaderEmailText.setText(teamInfo.getLeaderEmail());
            teamMembersText.setText(teamInfo.getTeamMembers());


        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

    }
}
