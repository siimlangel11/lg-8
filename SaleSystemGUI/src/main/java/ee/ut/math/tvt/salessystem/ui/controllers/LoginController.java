package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.HistoryStorage;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.WarehouseStorage;
import ee.ut.math.tvt.salessystem.ui.ControllerUtility;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.ActionEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    private static final Logger log = LogManager.getLogger(LoginController.class);
    private static final ControllerUtility utils = new ControllerUtility();

    private final HistoryStorage history;
    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;
    private final WarehouseStorage warehouse;



    @FXML
    private Button cashierBtn;
    @FXML
    private Button warehouseBtn;
    @FXML
    private Button managerBtn;

    public LoginController(SalesSystemDAO dao, ShoppingCart shoppingCart, HistoryStorage history, WarehouseStorage warehouse) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
        this.history = history;
        this.warehouse = warehouse;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) { }

    public void cashierButtonListener(ActionEvent event) { login("cashier", event); }

    public void warehouseButtonListener(ActionEvent event) {
        login("warehouse", event);
    }

    public void managerButtonListener(ActionEvent event) {
        login("manager", event);
    }




    private void login(String position, ActionEvent event) {
        // Get os specific file separator cus css files are up one dir
        String separator = System.getProperty("file.separator");

        Group root = new Group();
        Scene scene = new Scene(root, 600, 500, Color.WHITE);

        BorderPane borderPane = new BorderPane();
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        TabPane tabPane = new TabPane();

        try {
            // Initalize PurchaseController
            if (position.equals("cashier") || position.equals("manager")) {
                Tab purchaseTab = new Tab();
                purchaseTab.setText("Point-of-sale");
                purchaseTab.setClosable(false);
                purchaseTab.setContent(utils.loadControls("PurchaseTab.fxml", new PurchaseController(dao, shoppingCart)));
                tabPane.getTabs().add(purchaseTab);
            }
            // Initalize StockController
            if (position.equals("warehouse") || position.equals("manager")) {
                Tab stockTab = new Tab();
                stockTab.setText("Warehouse");
                stockTab.setClosable(false);
                stockTab.setContent(utils.loadControls("StockTab.fxml", new StockController(dao, warehouse)));
                tabPane.getTabs().add(stockTab);
            }
            // Initalize HistoryController
            if (position.equals("manager")) {
                // TODO: uncomment when history controller is finished
                Tab historyTab = new Tab();
                historyTab.setText("History");
                historyTab.setClosable(false);
                historyTab.setContent(utils.loadControls("HistoryTab.fxml", new HistoryController(dao, history)));
                tabPane.getTabs().add(historyTab);

            }
            // Initalize TeamController
            Tab teamTab = new Tab();
            teamTab.setText("Team");
            teamTab.setClosable(false);
            teamTab.setContent(utils.loadControls("TeamTab.fxml", new TeamController()));
            tabPane.getTabs().add(teamTab);

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }


        borderPane.setCenter(tabPane);
        root.getChildren().add(borderPane);

        // Create primary stage
        Stage primaryStage = new Stage();
        primaryStage.setTitle("Sales system");
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(getClass().getResource(String.join(separator, "..", "ApplicationTheme.css")).toExternalForm());

        // Close login window after login
        (((Button) event.getSource()).getScene().getWindow()).hide();
    }
}
