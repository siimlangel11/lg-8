package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.*;
import ee.ut.math.tvt.salessystem.logic.HistoryStorage;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.TeamInfo;
import ee.ut.math.tvt.salessystem.logic.WarehouseStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final HistoryStorage history;
    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final WarehouseStorage warehouse;
    private final TeamInfo team;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
        history = new HistoryStorage(dao);
        warehouse = new WarehouseStorage(dao);
        team = new TeamInfo(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String userPermission = login(in);
        printUsage(userPermission);
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase(), userPermission);
            System.out.println("Done. ");
        }
    }

    public String login(BufferedReader in) throws IOException {
        int choice = -1;
        while (choice != 1 && choice != 2 && choice != 3) {
            System.out.println("Login as...");
            System.out.println("(1) Cashier");
            System.out.println("(2) Warehouse worker");
            System.out.println("(3) Manager");
            System.out.print("> ");
            choice = Integer.parseInt(in.readLine().trim());
        }
        if (choice == 1) {
            return "cashier";
        } else if (choice == 2) {
            return "warehouse";
        } else {
            return "manager";
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showHistory(List<Purchase> purchases, BufferedReader bufferedReader) throws IOException {
        int i = 0;
        for (Purchase purchase: purchases) {
            System.out.println("(" + i + ") " + "Date: " + purchase.getDate() + " Time: " + purchase.getTime() + " Total " +purchase.getTotal());
            i++;
        }
        if (purchases.size() > 0) {
            System.out.println("Pick a purchase by number > ");
            int choice = Integer.parseInt(bufferedReader.readLine().trim());
            try {
                Purchase purchase = purchases.get(choice);
                for (HistoryItem item: purchase.getContents()) {
                    System.out.println("Id: " + item.getId() + " Name: " + item.getName() + " Price: " + item.getPrice() + " Quantity: " + item.getQuantity() + " Sum: " + item.getSum());
                }
            } catch (Exception e) {
                System.out.println("No such purchase");
            }
        }
    }

    private void showTeam() {
        try {
            TeamMembers teamInfo = new TeamMembers();
            System.out.println("Team name: " + teamInfo.getTeamName());
            System.out.println("Team leader: " + teamInfo.getLeaderName());
            System.out.println("Leader email: " + teamInfo.getLeaderEmail());
            System.out.println("Team members: " + teamInfo.getTeamMembers());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void promptHistory() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("---------History---------");
            System.out.println("(1) Show between dates");
            System.out.println("(2) Show last 10");
            System.out.println("(3) Show all");
            System.out.println("-------------------------");
            System.out.print("> ");
            int choice = Integer.parseInt(bufferedReader.readLine().trim());
            if (choice == 1) {
                while (true) {
                    System.out.println("Date example: 2016-08-07");
                    System.out.println("Enter start date > ");
                    String start = bufferedReader.readLine().trim();
                    System.out.println("Enter end date > ");
                    String end = bufferedReader.readLine().trim();
                    try {
                        LocalDate startDate = LocalDate.parse(start);
                        LocalDate endDate = LocalDate.parse(end);

                        showHistory(history.getBetweenDates(startDate, endDate), bufferedReader);
                    } catch (DateTimeParseException e) {
                        System.out.println("Faulty date entered");
                    }
                    return;
                }
            } else if (choice == 2) {
                showHistory(history.getLast10(), bufferedReader);
                return;
            } else if (choice == 3) {
                showHistory(history.getAllPurchases(), bufferedReader);
                return;
            }
        }
    }

    private void printUsage(String permission) {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("t\t\tShow team view");
        if (permission.equals("manager") || permission.equals("warehouse")) {
            System.out.println("w\t\tShow warehouse contents");

        }
        System.out.println("a " +
                (permission.equals("warehouse") ? "w\t" : "") +
                (permission.equals("manager") ? "w/c" : "") +
                (permission.equals("cashier") ? "c\t" : "") +
                "\tAdd stock item to " +
                (permission.equals("warehouse") ? "warehouse" : "") +
                (permission.equals("manager") ? "warehouse/cart" : "") +
                (permission.equals("cashier") ? "cart" : "") +
                "\n\t\tFormat: a " +
                (permission.equals("warehouse") ? "w" : "") +
                (permission.equals("manager") ? "w/c" : "") +
                (permission.equals("cashier") ? "c" : "") +
                " ID QUANTITY " +
                (permission.equals("cashier") ? "" : "NAME(in case of new item) PRICE(in case of new item)"));
        if (permission.equals("manager") || permission.equals("cashier")) {
            System.out.println("c\t\tShow cart contents");
            System.out.println("p\t\tPurchase the shopping cart");
            System.out.println("r\t\tReset the shopping cart");
            System.out.println("ri\t\tRemove item from shopping cart. Format: ri ID QUANTITY");
        }
        if (permission.equals("manager")) {
            System.out.println("k\t\tShow history");
        }
        System.out.println("exit\tExit the system.");
        System.out.println("-------------------------");
    }

    // Checks whether the logged in user has access to a command
    private boolean hasPermission(String commandToCheck, String permissionLevel) {
        String[] cashierCommands = new String[]{"a", "c", "p", "r", "ri"};
        String[] warehouseCommands = new String[]{"w", "a"};

        if (permissionLevel.equals("manager")) return true;

        if (permissionLevel.equals("cashier")) {
            for (String comm: cashierCommands) {
                if (comm.equals(commandToCheck)) {
                    return true;
                }
            }
        } else if (permissionLevel.equals("warehouse")) {
            for (String comm: warehouseCommands) {
                if (comm.equals(commandToCheck)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void processCommand(String command, String permission) throws IOException {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage(permission);
        else if (c[0].equals("t"))
            showTeam();
        else if (c[0].equals("q"))
            System.exit(0);
        else if(c[0].equals("exit"))
            System.exit(0);
        else if (c[0].equals("w") && hasPermission("w", permission))
            showStock();
        else if (c[0].equals("c") && hasPermission("c", permission))
            showCart();
        else if (c[0].equals("p") && hasPermission("p", permission)) {
            log.debug("Sale complete");
            cart.submitCurrentPurchase();
        } else if (c[0].equals("r") && hasPermission("r", permission)) {
            log.debug("Sale cancelled");
            cart.cancelCurrentPurchase();
        } else if(c[0].equals("ri") && hasPermission("ri", permission)) {
            try {
                boolean found = false;
                Long idx = Long.parseLong(c[1]);
                int quantity = Integer.parseInt(c[2]);
                StockItem stockItem = dao.findStockItemByID(idx);
                if (stockItem != null){
                    found = cart.removeItem(stockItem, quantity);
                    if (!found) {
                        System.out.println("No such item in cart right now or incorrect quantity to remove entered.");
                    }
                } else {
                    System.out.println("Item with ID " + idx + " does not exist.");
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                System.out.println("No such item in cart right now.");
            } catch (IndexOutOfBoundsException e) {
                if (c.length == 1){
                    System.out.println("Please specify item id and quantity of item to remove.");
                }
                if (c.length == 2){
                    System.out.println("Please specify quantity of item to remove.");
                }
            }
        } else if (c[0].equals("a") && c.length >= 4 && hasPermission("a", permission)) {
            if (c[1].equals("w") && hasPermission("w", permission)) {
                if (c.length == 4) {
                    try {
                        long idx = Long.parseLong(c[2]);
                        long amount = Integer.parseInt(c[3]);
                        if (amount < 0 || amount > Integer.MAX_VALUE / 2) {
                            throw new SalesSystemException("Wrong quantity input");
                        }
                        StockItem current = dao.findStockItemByID(idx);
                        if (current != null) {
                            StockItem item = new StockItem();
                            item.setId(current.getId());
                            item.setName(current.getName());
                            item.setDescription(current.getDescription());
                            item.setQuantity( (int)amount);
                            item.setPrice(current.getPrice());
                            log.info("Added item to warehouse: " + item.getName());
                            warehouse.addItem(item);
                        } else {
                            System.out.println("no stock item with id " + idx);
                        }
                    } catch (SalesSystemException | NoSuchElementException e) {
                        log.error(e.getMessage(), e);
                    }
                } else if (c.length >= 6) {
                    StockItem newItem = new StockItem();
                    newItem.setId(Long.parseLong(c[2]));
                    newItem.setQuantity(Integer.parseInt(c[3]));
                    String name = "";
                    for (int i = 4; i < c.length - 1; i++) {
                        name += c[i] + " ";
                    }
                    newItem.setName(name);
                    newItem.setPrice(Double.parseDouble(c[c.length - 1]));
                    if (newItem.getId() < 0) {
                        System.out.println("Incorrect ID input");
                    } else if (newItem.getQuantity() < 0) {
                        System.out.println("Incorrect quantity input");
                    } else if (newItem.getPrice() < 0.0) {
                        System.out.println("Incorrect price input");
                    } else if (warehouse.contains(newItem)) {
                        System.out.println("Item with this ID already exists.");
                    } else {
                        log.info("Added item to warehouse: " + newItem.getName());
                        warehouse.addItem(newItem);
                    }
                }
            } else if (c[1].equals("c") && c.length == 4 && hasPermission("c", permission)) {
                try {
                    long idx = 0;
                    long amount = 0;
                    try {
                        idx = Long.parseLong(c[2]);
                        amount = Integer.parseInt(c[3]);
                    } catch (NumberFormatException e) {
                        throw new SalesSystemException("Wrong quantity or index");
                    }
                    StockItem item = dao.findStockItemByID(idx);
                    if (item != null) {
                        if (item.getQuantity() - amount >= 0 && amount <= item.getQuantity()) {
                            boolean added = cart.addItem(new SoldItem(item,(int) Math.min(amount, item.getQuantity())));
                            if (!added) {
                                throw new SalesSystemException("Not enough items in the warehouse");
                            }

                        } else {
                            System.out.println("Not enough items in warehouse");
                        }
                    } else {
                        System.out.println("No stock item with id " + idx);
                    }
                } catch (SalesSystemException | NoSuchElementException e) {
                    log.error(e.getMessage(), e);
                }
            }
        } else if (c[0].equals("k") && hasPermission("k", permission)) {
            promptHistory();
        } else {
            System.out.println("unknown command");
        }
    }

}
